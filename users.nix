{ config, ... }:

let options = import ./options.nix;

in {
  programs."${options.user.shell}".enable = true;

  users.users = {
    "${options.user.name}" = {
      isNormalUser = true;
      extraGroups = [ "wheel" "adm" "video" "audio" "docker" ];
      uid = options.user.uid;
      shell = "/run/current-system/sw/bin/${options.user.shell}";
    };
  };
}
