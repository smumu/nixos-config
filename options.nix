rec {

  # you should fill this arguments

  type = "laptop"; # desktop, laptop or server

  user = {
    name  = "samuel";
    uid   = 1000;
    shell = "zsh";
  };

  hostname = "sam-computer";
  timezone = "Europe/Paris";
  layout   = "us";
  location = {
    latitude  = "48.866667";
    longitude = "2.333333";
  };

  screen = {
    width  = "1920";
    height = "1080";
  };

  boot = {
    disk = "/dev/sda";
    resolution = screen.width + "x" + screen.height;
  };


  # maybe you don't need to touch the following options

  ssh = if type == "server" then true else false;
  desktop = if type == "laptop" || type == "desktop" then true else false;
}
