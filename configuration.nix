# Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’)

{ config, lib, pkgs, ... }:

with lib;

let options = import ./options.nix;

in {

  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./users.nix
      ./containers.nix
      ./pkgs.nix
    ];

  boot.kernel.sysctl = {
    "vm.swappiness" = 10;
  };

  # Use the systemd boot loader.
  boot.loader.grub.enable = false;
  boot.loader.systemd-boot.enable = true;

  networking.networkmanager.enable = true;
  networking.hostName = options.hostname; # Define your hostname.
  networking.wireless.enable = options.desktop;  # Enables wireless support via wpa_supplicant.

  hardware.bluetooth.enable = options.desktop;
  hardware.pulseaudio.package = if options.desktop then pkgs.pulseaudioFull else pkgs.pulseaudioLight;

  # Select internationalisation properties.
  i18n = {
    consoleKeyMap = options.layout;
    defaultLocale = "fr_FR.UTF-8";
    supportedLocales = [ "en_US.UTF-8/UTF-8" "fr_FR.UTF-8/UTF-8" ];
  };

  # Set your time zone.
  time.timeZone = options.timezone;

  # Enable the OpenSSH daemon.
  services.openssh.enable = options.ssh;

  # Enable ClamAV
  services.clamav.daemon.enable = true;
  services.clamav.updater.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = options.desktop;

  # Enable the X11 windowing system.
  services.xserver.enable = options.desktop;
  services.xserver.layout = options.layout;

  # interface
  services.xserver.displayManager.gdm.enable = options.desktop;
  services.xserver.desktopManager.gnome3.enable = options.desktop;
  services.gnome3 = mkIf options.desktop {
    gnome-documents.enable       = true;
    gnome-online-accounts.enable = true;
    gnome-online-miners.enable   = true;
    gvfs.enable  = true;
    sushi.enable = true;
  };
  services.xserver.libinput.enable = options.desktop;

  services.redshift = {
    enable    = options.desktop;
    latitude  = options.location.latitude;
    longitude = options.location.longitude;
    temperature = {
      day   = 6000;
      night = 4000;
    };
  };

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "16.09";
}
