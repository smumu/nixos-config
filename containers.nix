{ config, ... }:

let options = import ./options.nix;

in {
  virtualisation.lxc.enable = true;
  virtualisation.docker.enable = true;

  containers.www = {
    autoStart = true;

    bindMounts.www = {
      hostPath   = "/home/${options.user.name}/dev/www";
      mountPoint = "/var/www";
      isReadOnly = true;
    };

    config =
      { config, pkgs, ... }:
      {
        imports = [ ./users.nix ];

        time.timeZone = options.timezone;

        services.nginx.enable = true;
        services.nginx.httpConfig = ''
          server {
            listen 80 default_server;
            root /var/www;

            location / {
              try_files $uri $uri/ index.html index.php;
              index index.php index.html;
              autoindex on;
            }
          }
        '';
      };
  };
}
