{ lib, nixpkgs, pkgs, ... }:

with lib;

let options = import ./options.nix;

in {
  environment.systemPackages = with pkgs; mkMerge [
    [
      wget
      vim
    ]
    (mkIf options.desktop [
      arc-gtk-theme
      numix-icon-theme
      numix-icon-theme-circle
      xorg.xcursorthemes
    ])
  ];
}
